#!/bin/bash
echo Rig name:
read rigname
echo Rig name is $rigname

username=$USER
ethaddress="0x8C1f70A206ffF010b1209C59d51871e8d4C6D581"
pooladdress="us-eth.2miners.com:12020"

install_sensors() {
	sudo apt update
	sudo apt install lm-sensors
}

install_screen() {
	sudo apt update
	sudo apt install screen
}

install_open_drivers() {
	sudo apt update

	echo "Installing amdgpu-pro..."
	amdgpudir=./amdgpu-pro
	amdgpudriver=amdgpu-pro-20.30-1109583-ubuntu-20.04
	rm -rf $amdgpudir &>/dev/null
	mkdir $amdgpudir
	tar -xf $amdgpudriver.tar.xz -C $amdgpudir
	amdgpu-pro-uninstall
	$amdgpudir/$amdgpudriver/amdgpu-install
	sudo dpkg -i $amdgpudir/$amdgpudriver/amdgpu-pro-core_20.30-1109583_all.deb
	sudo dpkg -i $amdgpudir/$amdgpudriver/ocl-icd-libopencl1-amdgpu-pro_20.30-1109583_amd64.deb
	sudo dpkg -i $amdgpudir/$amdgpudriver/clinfo-amdgpu-pro_20.30-1109583_amd64.deb
	sudo dpkg -i $amdgpudir/$amdgpudriver/opencl-amdgpu-pro-comgr_20.30-1109583_amd64.deb
	sudo dpkg -i $amdgpudir/$amdgpudriver/opencl-amdgpu-pro-icd_20.30-1109583_amd64.deb
	sudo dpkg -i $amdgpudir/$amdgpudriver/libdrm2-amdgpu_2.4.100-1109583_amd64.deb
	echo "done"

	sudo usermod -a -G video $username
}

install_drivers() {
	sudo apt update

	echo "Installing amdgpu-pro..."
	amdgpudir=./amdgpu-pro
	amdgpudriver=amdgpu-pro-20.30-1109583-ubuntu-20.04
	rm -rf $amdgpudir &>/dev/null
	mkdir $amdgpudir
	tar -xf $amdgpudriver.tar.xz -C $amdgpudir
	amdgpu-pro-uninstall 
	$amdgpudir/$amdgpudriver/amdgpu-pro-install --opencl=pal,legacy
	echo "done"

	sudo usermod -a -G video $username
}

install_ethminer() {
	echo "Installing ethminer..."
	ethminerdir=$(pwd)/ethminer
	sudo rm -rf $ethminerdir &>/dev/null
	mkdir $ethminerdir

	echo "Extracting ethminer..."
	mkdir $ethminerdir/ethminer-0.18.0-cuda-9-linux-x86_64
	tar -xf ethminer-0.18.0-cuda-9-linux-x86_64.tar.gz -C $ethminerdir/ethminer-0.18.0-cuda-9-linux-x86_64
	sudo chmod a+x $ethminerdir/ethminer-0.18.0-cuda-9-linux-x86_64/bin/ethminer
	echo "done"

	echo "Copying ethminer processor script..."
	cp ethminer_monitor.py $ethminerdir/
	echo "done"

	echo "Creating start_ethminer script..."
	cat > $ethminerdir/start_ethminer.sh <<EOF
#!/bin/sh
echo "[\$(date +"%T")] Starting ETH Miner! :)" >> ./service.log
export NO_COLOR=1
$ethminerdir/ethminer-0.18.0-cuda-9-linux-x86_64/bin/ethminer -P stratum1+ssl://$ethaddress.$rigname@$pooladdress 2>&1 | tee -a $ethminerdir/application.log | python3 $ethminerdir/ethminer_monitor.py
echo "[\$(date +"%T")] ETH Miner stopped. :(" >> ./service.log
EOF
	sudo chmod a+x $ethminerdir/start_ethminer.sh
	sudo chown -R $username:$username $ethminerdir
	echo "done"

	echo "Creating service entry..."
	sudo cat > $ethminerdir/ethminer.service <<EOF
[Unit]
Description=ETH Miner
Requires=network.target
After=network.target

[Service]
User=$username
WorkingDirectory=$ethminerdir
ExecStart=/usr/bin/screen -DmS ethminer $ethminerdir/start_ethminer.sh
ExecStop=/usr/bin/screen -XS ethminer quit
Restart=always
RestartSec=4

[Install]
WantedBy=multi-user.target
EOF

	sudo rm /etc/systemd/system/ethminer.service &>/dev/null
	sudo mv $ethminerdir/ethminer.service /etc/systemd/system/
	sudo systemctl daemon-reload
	echo "done"

	echo "ethminer installed!"
}

install_phoenixminer() {
	echo "Installing phoenixminer..."
	phoenixminerdir=$(pwd)/phoenixminer
	sudo rm -rf $phoenixminerdir &>/dev/null
	mkdir $phoenixminerdir

	echo "Extracting phoenixminer..."
	mkdir $phoenixminerdir/PhoenixMiner_5.5c_Linux
	unzip PhoenixMiner_5.5c_Linux.zip -d $phoenixminerdir
	chmod a+x $phoenixminerdir/PhoenixMiner_5.5c_Linux/PhoenixMiner
	echo "done"

	echo "Creating start_phoenixminer script..."

	cat > $phoenixminerdir/start_phoenixminer.sh <<EOF
#!/bin/sh
echo "[\$(date +"%T")] Starting Phoenix Miner! :)" >> ./application.log
$phoenixminerdir/PhoenixMiner_5.5c_Linux/PhoenixMiner -pool stratum1+ssl://$pooladdress -wal 0x8C1f70A206ffF010b1209C59d51871e8d4C6D581.$rigname | tee $phoenixminerdir/application.log
echo "[\$(date +"%T")] Phoenix Miner stopped. :(" >> ./application.log
EOF
	chmod a+x $phoenixminerdir/start_phoenixminer.sh
	sudo chown -R $username:$username $phoenixminerdir
	echo "done"

	echo "Creating service entry..."

	cat > $phoenixminerdir/phoenixminer.service <<EOF
[Unit]
Description=ETH Miner
Requires=network.target
After=network.target

[Service]
User=$username
WorkingDirectory=$phoenixminerdir
ExecStart=/usr/bin/screen -DmS phoenixminer $phoenixminerdir/start_phoenixminer.sh
ExecStop=/usr/bin/screen -XS phoenixminer quit
Restart=always
RestartSec=4

[Install]
WantedBy=multi-user.target
EOF

	sudo rm /etc/systemd/system/phoenixminer.service &>/dev/null
	sudo mv $phoenixminerdir/phoenixminer.service /etc/systemd/system/
	sudo systemctl daemon-reload
	echo "done"

	echo "phoenixminer installed!"
}


install_screen

install_sensors

# install_open_drivers

install_drivers

install_ethminer

install_phoenixminer
