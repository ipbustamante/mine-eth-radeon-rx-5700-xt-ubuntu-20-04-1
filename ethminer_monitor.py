import re
import sys
import subprocess


JOB_LINE_PATTERN = re.compile(" i (?P<timestamp>\d{2,}:\d{2,}:\d{2,}) (\S+) Job: (\S+) (block \d+) (\S+) \[(\d+\.\d+\.\d+\.\d+:\d+)\]")
SHARE_FOUND_PATTERN = re.compile("cl (?P<timestamp>\d{2,}:\d{2,}:\d{2,}) (\S+)\s+Job: (\S+)(…|\.\.\.) Sol: (\S+)")
HASHRATE_PATTERN = re.compile(" m (?P<timestamp>\d{2,}:\d{2,}:\d{2,}) (\S+) (?P<hours_runnig>\d+:\d+) A(?P<shares_accepted>\d+) (?P<hashrate>[\d\.]+) (?P<hashrate_unit>\S+) - \S+ ([\d\.]+)")
SHARE_ACCEPTED_PATTERN = re.compile(" i (?P<timestamp>\d{2,}:\d{2,}:\d{2,}) (\S+) \*\*Accepted  \d+ ms. (\S+) \[(\d+\.\d+\.\d+\.\d+:\d+)\]")


def parse_hashrate(hashrate, hashrate_unit):
    hashrate_number = float(hashrate)
    if hashrate_unit == "Mh":
        hashrate_number *= 1000
    elif hashrate_unit == "Gh":
        hashrate_number *= 1000000
    return hashrate_number


def format_hashrate(hashrate):
    hashrate_string = ""
    if hashrate >= 1000000:
        hashrate_string = "{:10.2f}Gh".format(hashrate/1000000)
    elif hashrate >= 1000:
        hashrate_string = "{:10.2f}Mh".format(hashrate/1000)
    elif hashrate < 1000:
        hashrate_string = "{:10.2f}h".format(hashrate)
    return hashrate_string
    

class EthminerMonitor:
    def __init__(self):
        self.shares_found = 0
        self.shares_accepted = 0
        self.jobs_received = 0
        self.hours_running = "0:00"
        self.time_current = "00:00:00"
        self.time_last_job = "none yet"
        self.time_last_share_found = "none yet"
        self.time_last_share_accepted = "none yet"
        self.hashrate_last_minute_values = [0] * 12 # 12 a minute, every 5 seconds
        self.hashrate_last_minute_average = 0
        self.hashrate_last_hour_values = [0] * 720 # 720 an hour, every 5 seconds
        self.hashrate_last_hour_average = 0
        self.hashrate_current = 0

    def parse_line(self, line):
        line_type = line[0:2]
        if line_type == " i":
            self.parse_i_line(line)
        elif line_type == " m":
            self.parse_m_line(line)
        elif line_type == "cl":
            self.parse_cl_line(line)

    def parse_i_line(self, line):
        match = JOB_LINE_PATTERN.match(line)
        if match:
            self.jobs_received += 1
            self.time_last_job = match.group('timestamp')
            self.time_current = match.group('timestamp')
            return

        match = SHARE_ACCEPTED_PATTERN.match(line)
        if match:
            self.shares_accepted += 1
            self.time_last_share_accepted = match.group('timestamp')

    def parse_m_line(self, line):
        match = HASHRATE_PATTERN.match(line)
        if match:
            self.hashrate_current = parse_hashrate(match.group("hashrate"), match.group("hashrate_unit"))

            self.hashrate_last_minute_values.append(self.hashrate_current)
            self.hashrate_last_minute_values = self.hashrate_last_minute_values[1:]
            self.hashrate_last_minute_average = sum(self.hashrate_last_minute_values) / 12

            self.hashrate_last_hour_values.append(self.hashrate_current)
            self.hashrate_last_hour_values = self.hashrate_last_hour_values[1:]
            self.hashrate_last_hour_average = sum(self.hashrate_last_hour_values) / 720

            self.hours_running = match.group("hours_runnig")

    def parse_cl_line(self, line):
        match = SHARE_FOUND_PATTERN.match(line)
        if match:
            self.shares_found +=1
            self.time_last_share_found = match.group('timestamp')
            self.time_current = match.group('timestamp')

    def print_miner_monitor(self):
        print(
            f"[{self.time_current}] "+
            f"last job received: {self.time_last_job}; "+
            f"last share found: {self.time_last_share_found}; "+
            f"last share accepted: {self.time_last_share_accepted}"
        )
        print(
            f"Jobs received: {self.jobs_received}; "+
            f"Shares found: {self.shares_found}; "+
            f"Shares accepted: {self.shares_accepted}"
        )
        print(
            f"{self.hours_running}, "+
            f"{format_hashrate(self.hashrate_current)}; "+
            f"{format_hashrate(self.hashrate_last_minute_average)} 1m avg; "+
            f"{format_hashrate(self.hashrate_last_hour_average)} 1h avg"
        )


def main():
    # f = open("ethminer-sample.log", "r")
    miner_monitor = EthminerMonitor()
    # proc = subprocess.Popen(["/home/ivpabufi/mine-eth/ethminer/ethminer-0.18.0-cuda-9-linux-x86_64/bin/ethminer","-P stratum1+ssl://0x8C1f70A206ffF010b1209C59d51871e8d4C6D581.azaghal@us-eth.2miners.com:12020"],stdout=subprocess.PIPE)
    # proc = subprocess.Popen(["cat","/media/virtualbox-shared/process_ethminer_output/ethminer-sample.log"], stdout=subprocess.PIPE)
    # for line in proc.stdout:
        # print(line.decode().rstrip())
        # miner_monitor.parse_line(line.decode().rstrip())
    for line in sys.stdin:
    # while True:
        # line = sys.stdin.readline()
        miner_monitor.parse_line(line.rstrip())
        miner_monitor.print_miner_monitor()

        if miner_monitor.hours_running > "0:04" and (miner_monitor.hashrate_last_minute_average >= 1000000 or miner_monitor.hashrate_last_minute_average <= 1000):
            raise Exception("Hashrate outside of expected bounds!")

        print("\033[F\033[F\033[F",end="")
    print("\n\n\n")


if __name__ == "__main__":
    main()
