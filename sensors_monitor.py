import pprint
import re
import sys

MIN_FAN_SPEED = 1500

TEMPERATURE_LINE_PATTERN = re.compile("(?P<sensor_name>\S+):\s+[+-]*(?P<sensor_temperature>[\d\.]+)°C.*")
POWER_LINE_PATTERN = re.compile("(?P<sensor_name>\S+):\s+(?P<power>[\d\.]+) (?P<unit>m?W).*")
FAN_SPEED_LINE_PATTERN = re.compile("(?P<sensor_name>\S+):\s+(?P<fan_speed>\d+) RPM.*")


def main():
    pci_adapter_sensor_map = {}
    current_pci_adapter = None
    previous_line = None

    for line in sys.stdin:
        line = line.rstrip()
        if line == "Adapter: PCI adapter":
            current_pci_adapter = previous_line
            pci_adapter_sensor_map[current_pci_adapter] = {}

        if current_pci_adapter is not None:
            match = TEMPERATURE_LINE_PATTERN.match(line)
            if match:
                pci_adapter_sensor_map[current_pci_adapter][match.group("sensor_name")] = {
                    "temperature": float(match.group("sensor_temperature")),
                    "unit": "°C"
                }

            match = FAN_SPEED_LINE_PATTERN.match(line)
            if match:
                fan_speed = int(match.group("fan_speed"))
                pci_adapter_sensor_map[current_pci_adapter][match.group("sensor_name")] = {
                    "fan_speed": fan_speed,
                    "unit": "RPM"
                }

            match = POWER_LINE_PATTERN.match(line)
            if match:
                pci_adapter_sensor_map[current_pci_adapter][match.group("sensor_name")] = {
                    "power": float(match.group("power")),
                    "unit": match.group("unit")
                }

        previous_line = line

    pprint.pprint(pci_adapter_sensor_map)

    if pci_adapter_sensor_map["amdgpu-pci-0900"]["fan1"]["fan_speed"] < MIN_FAN_SPEED:
        raise Exception('Fan speed too low!')
        

if __name__ == "__main__":
    main()