# Mine ETH on Ubuntu 20.04.1 with Radeon
This guide is provided as-is.
Feel free to send pull-requests for the script or for the README, but I will only incorporate changes I've tested myself (ETA 1-365 days).

This has been tested on the following graphic cards:

* AMD Radeon RX 5600 XT
* AMD Radeon RX 5700 XT

## Download Ubuntu (ETA depends on your internet)
[ubuntu-20.04.1-desktop-amd64.iso](http://old-releases.ubuntu.com/releases/20.04.1/ubuntu-20.04.1-desktop-amd64.iso)

## Create Bootable Pendrive (ETA depends on your computer)
* On Windows: Download [Rufus](https://rufus.ie/)

## Install Ubuntu (ETA depends on your computer)
1. Plug the pendrive.
2. (Maybe) Change BIOS boot order to boot from the pendrive.
3. Turn on computer.
4. Follow steps to install.

## Download AMD Radeon Drivers
[amdgpu-pro-20.30-1109583-ubuntu-20.04.tar.xz](https://drivers.amd.com/drivers/linux/amdgpu-pro-20.30-1109583-ubuntu-20.04.tar.xz)

## Download PhoenixMiner 5.5c
Download [Phoenix Miner 5.5c](https://phoenixminer.org/download/)

## Download ethminer 0.18.0
Download [ethminer 0.18.0](https://github.com/ethereum-mining/ethminer/releases/tag/v0.18.0)

## Use the Quick Start Script
1. [Download install_eth_mining_quickstart.sh](./install_eth_mining_quickstart.sh)
2. Make sure everything you downloaded is in the same folder.
3. Run script
  ```
./install_eth_mining_quickstart.sh
  ```

## Reboot

## Want to thank me?
### Run ethminer
```
sudo systemctl start ethminer
```

### Or run PhoenixMiner
```
sudo systemctl start phoenixminer
```

### Leave running proportionally to your gratitude
### And a little longer if possible :)

## Mine some ETH for yourself
1. Modify install_eth_mining_quickstart.sh
2. Change `ethaddress` to your own wallet address.
3. (Optional) change `pooladdress` to a different pool.
4. (Optional) comment out installing Radeon drivers and save some time.
5. Run script again
   ```
 ./install_eth_mining_quickstart.sh
   ```
6. (Re)start ethminer or PhoenixMiner
7. Wait.
8. Seriously, like 10-60 minutes.
9. (Optional) Run ethminer at boot `sudo systemctl enable ethminer`. Or un phoenixminer at boot `sudo systemctl enable phoenixminer`. Don't do both, pick one. `sudo systemctl disable (eth|phoenix)miner` to disable one of them
10. (Optional) Check on your mining progress:
   * On 2miners: https://eth.2miners.com/account/<your wallet address>
